﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseMenuLevel2 : MonoBehaviour
{

    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;

    // Checks if the game is paused or not
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
            Debug.Log("Paused");
        }
    }

    void Pause()
    {

        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Resume()
    {

        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Debug.Log("Resume");
    }

    public void ReplayLevel()
    {
        Debug.Log("Restarts level");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
    }

    public void MainMenu()
    {
        Debug.Log("Open Main Manu");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }

    // Add new levels here
    public void ChooseLevelOne()
    {

        Debug.Log("Starting Level 1");
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
    }

    public void ChooseLevelTwo()
    {

        Debug.Log("Starting Level 2");
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
