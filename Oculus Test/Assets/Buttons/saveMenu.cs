﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class saveMenu : MonoBehaviour
{
    public void Yes() {

        Debug.Log("Saving game, quitting game..");
        // Save game code here
        Application.Quit();
    }
    public void No() {

        Debug.Log("Not Saving game, quitting game..");
        Application.Quit();
    }
}
