﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarScript : MonoBehaviour
{
    public AudioSource collect;
    
    // Start is called before the first frame update
    void Start()
    {
        collect = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Player")
        {
            collect.Play();
            StartCoroutine("WaitForSec");
        }
        
    }
    IEnumerator WaitForSec()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);

    }
}
