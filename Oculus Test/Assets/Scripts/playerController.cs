﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    // Inputs
    public float maxSpeed = 4;
    public float speedVertical;
    public float speedHorizontal;

    public float overallSpeed;
    public float gravity = 0f;
    private CharacterController controller;

    private Vector3 playerMotion = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        //rb = GetComponent<Rigidbody>();
       controller = GetComponent<CharacterController>();
    
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (controller.isGrounded)
        {
            //moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            //moveDirection *= runSpeed;

            // Manipulate player direction
            transform.RotateAroundLocal(Vector3.up, maxSpeed * 0.1f * (speedHorizontal + Input.GetAxis("Horizontal")) * Time.fixedDeltaTime);

            playerMotion = maxSpeed*(speedVertical + Input.GetAxis("Vertical")) * transform.TransformDirection(Vector3.forward);
            
            //Debug.Log(moveDirection);
        }

        // Gravity
        playerMotion.y -= gravity;

        // Move player
        controller.Move(playerMotion * Time.fixedDeltaTime);
        overallSpeed = controller.velocity.magnitude;

    }


    private void Update()
    {
     
    }
}
