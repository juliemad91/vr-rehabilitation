﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;
using MathNet.Numerics.LinearAlgebra.Factorization;


public class TerrainObserver : MonoBehaviour
{
    // Settings
    public bool estimateLocalPlane = true;
    public float h0;


    // TxData
    public float h;
    public float rotX;
    public float rotZ;

    // RxData
    public float speedVertical;
    public float speedHorizontal;

    public Vector3 nVector;
    public float checkDistance = 2;
    public float checkRadius = 0.5f;
    public int nChecks = 20;
    public int nHits = 0;

    // UDP related
    public int nRecv;
    public byte[] rxBuffer, txBuffer;

    private IPEndPoint remoteEP, localEP;

    private Vector3[] origin, direction;

    private RaycastHit[] hits;

    private Matrix<float> A, N, V;

    private UdpClient socket;

    // Start is called before the first frame update
    void Start()
    {
        socket = new UdpClient(4000);

        socket.BeginReceive(new AsyncCallback(OnUdpData), socket);



    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Contruct raycast hit array
        hits = new RaycastHit[nChecks];
        origin = new Vector3[nChecks];
        direction = new Vector3[nChecks];

        // Terrain heigth
        h = (transform.position + transform.TransformDirection(Vector3.down)).y - h0;

        Vector3 v = checkRadius*Vector3.forward;

        // Find plane with Raycast
        http://thehiddensignal.com/unity-angle-of-sloped-ground-under-player/

        // Reset hit count
        nHits = 0;
        for (int i = 0; i < nChecks; i++)
        {
            // Raycast check origin in global space
            origin[i] = transform.position + transform.TransformDirection(
                0.5f * Vector3.down + Quaternion.Euler(0, (float)i / nChecks * 360 , 0) * v
            );

            // Raycast check direction in global space
            direction[i] = transform.TransformDirection(Vector3.down);

            // Cast ray
            if (Physics.Raycast(origin[i], direction[i], out hits[i], checkDistance))
            {
                // Count number of hits
                nHits = nHits + 1;

                // Draw result
                Debug.DrawRay(origin[i], direction[i] * hits[i].distance, Color.red);

            }
        }

        // Estimate plane based on 3 or more hitpoints
        if (nHits >= 3)
        {
            A = Matrix<float>.Build.Dense(nHits, 4);

            // Fill data
            for (int i = 0; i < nHits; i++)
            {
                // Calibration of the Norwegian motion laboratory using conformal geometric algebra, Olav Heng med ml.
                // https://dl.acm.org/doi/abs/10.1145/3095140.3097285?casa_token=a4BDviqGwywAAAAA:Xa7wPapL0sY-fAxfMJvYaKG7JTrUP5AlYQvHikOBizZa2iLkgZfVGdlpZNAlrXSJPqbIaIzEeByi
                // https://github.com/sondre1988/matlab-functions/blob/master/src/PlaneFitCGA.m

                // Hit point in global space and local space
                Vector3 hitPointGlobal = origin[i] + direction[i] * hits[i].distance;
                Vector3 hitPointLocal = transform.InverseTransformPoint(hitPointGlobal);

                Vector3 point;
                if (estimateLocalPlane)
                {
                    point = hitPointLocal;
                }
                else
                {
                    point = hitPointGlobal;
                }

                // Stack hit points
                A[i, 0] = point.x;
                A[i, 1] = point.y;
                A[i, 2] = point.z;
                A[i, 3] = -1;

                // Solve using Eigenvalue decomposition
                N = A.Transpose() * A;
                Evd<float> evd = N.Evd();

                V = evd.EigenVectors;

                // Get normal vector
                Vector<float> sol = V.Column(0);

                // Fix signs
                if (sol[1] < 0)
                {
                    sol = -sol;
    
                }

                nVector.x = sol[0];
                nVector.y = sol[1];
                nVector.z = sol[2];

                nVector.Normalize();

                // Calculate angles of plane relative to world
                rotX = Mathf.Atan2(nVector.z, nVector.y);
                rotZ = Mathf.Atan2(nVector.x, nVector.y);


            }
        }

        // Send data to Python Middleware
        MemoryStream txStream = new MemoryStream();
        BinaryWriter txWriter = new BinaryWriter(txStream);

        // Encode message to byte array
        txWriter.Write(h);
        txWriter.Write(rotX);
        txWriter.Write(rotZ);
        
        txBuffer = txStream.ToArray();

        // Set speed on player
        playerController player = GetComponent<playerController>();
        player.speedHorizontal = speedHorizontal;
        player.speedVertical = speedVertical;

    }


    void OnUdpData(IAsyncResult result)
    {
        // this is what had been passed into BeginReceive as the second parameter:
        UdpClient socket = result.AsyncState as UdpClient;

        // Recieve data from remote source
        rxBuffer = socket.EndReceive(result, ref remoteEP);

        // Convert data
        MemoryStream rxStream = new MemoryStream(rxBuffer);
        BinaryReader rxReader = new BinaryReader(rxStream);

        speedVertical = rxReader.ReadSingle();
        speedHorizontal = rxReader.ReadSingle();

        
        // Echo data back to remote
        socket.Send(txBuffer, txBuffer.Length, remoteEP);

        // Schedule the next receive operation once reading is done:
        socket.BeginReceive(new AsyncCallback(OnUdpData), socket);

        // Increment counter
        nRecv = nRecv + 1;
    }

    private void OnApplicationQuit()
    {
        socket.Close();
    }
}
