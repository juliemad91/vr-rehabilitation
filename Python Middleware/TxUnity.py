import ctypes

class TxUnity(ctypes.Structure):
    _fields_ = [
        ('speedVertical', ctypes.c_float), # Egentlig velocity
        ('speedHorizontal', ctypes.c_float) # Egentlig rotVelocity
    ]
    