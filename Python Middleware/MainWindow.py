# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(872, 574)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.plot = GraphicsLayoutWidget(self.centralwidget)
        self.plot.setGeometry(QtCore.QRect(90, 20, 761, 511))
        self.plot.setObjectName("plot")
        self.speedVertical = QtWidgets.QSlider(self.centralwidget)
        self.speedVertical.setGeometry(QtCore.QRect(20, 120, 22, 271))
        self.speedVertical.setMinimum(-100)
        self.speedVertical.setMaximum(100)
        self.speedVertical.setOrientation(QtCore.Qt.Vertical)
        self.speedVertical.setObjectName("speedVertical")
        self.speedHorizontal = QtWidgets.QSlider(self.centralwidget)
        self.speedHorizontal.setGeometry(QtCore.QRect(50, 120, 22, 271))
        self.speedHorizontal.setMinimum(-100)
        self.speedHorizontal.setMaximum(100)
        self.speedHorizontal.setOrientation(QtCore.Qt.Vertical)
        self.speedHorizontal.setObjectName("speedHorizontal")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 872, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))

from pyqtgraph import GraphicsLayoutWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

