from PyQt5 import QtWidgets
from PyQt5 import QtCore
import pyqtgraph as pg
import numpy as np
import socket
from ctypes import *
import time

from MainWindow import Ui_MainWindow
from RealTimePlot import RealTimePlot

# PLC UDP Data Types import
from RxUdp import RxUdp
from TxUdp import TxUdp

# Unity data types
from RxUnity import RxUnity
from TxUnity import TxUnity

# PyQtGraph settings
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
pg.setConfigOptions(antialias=True)

# Usage Enum
E_MOTIONLAB = 0
E_LABVIEW = 1

class RemoteInterface(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(RemoteInterface, self).__init__()
        self.gui = Ui_MainWindow()
        self.gui.setupUi(self)

        # Define usage mode
        # self.eMode = E_MOTIONLAB
        self.eMode = E_LABVIEW

        # Unity Socket
        self.unity = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.unity.bind(('127.0.0.1', 4001))
        self.unity.setblocking(False)
        self.rxUnity = RxUnity()
        self.txUnity = TxUnity()

        # Plc Socket (Use towards motionlab or master students)
        self.plc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.plc.setblocking(False)

        if self.eMode == E_MOTIONLAB:
            self.plc.bind(('192.168.90.60', 50060))

            # Send and recieve data to Motion Lab directly
            self.txData = TxUdp()
            self.rxData = RxUdp()

        elif self.eMode == E_LABVIEW:
            self.plc.bind(('192.168.90.160', 50160))

            # Send and recieve unity data to LabVIEW
            self.txData = RxUnity()
            self.rxData = TxUnity()

        # Connect speed sliders
        self.gui.speedVertical.valueChanged.connect(self.onSpeedVertical)
        self.gui.speedVertical.sliderReleased.connect(lambda: self.gui.speedVertical.setValue(0))

        self.gui.speedHorizontal.valueChanged.connect(self.onSpeedHorizontal)
        self.gui.speedHorizontal.sliderReleased.connect(lambda: self.gui.speedHorizontal.setValue(0))

        # Udp Read/Write thread
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(50)

        # Initial time
        self.t0 = time.time()

        # Real-time plots
        self.plot1 = RealTimePlot(self.gui.plot.addPlot())
        self.plot1.plot.setLabel('left', 'Position', 'm')
        self.plot1.plot.setYRange(-0.2, 0.2)
        self.plot1.add_curves(['r'], ['h'])
        self.plot1.time_range = 10

        self.gui.plot.nextRow()
        self.plot2 = RealTimePlot(self.gui.plot.addPlot())
        self.plot2.plot.setLabel('left', 'Angle', 'deg')
        self.plot2.plot.setYRange(-6, 6)
        self.plot2.add_curves(['r', 'g'], ['rotX', 'rotZ'])
        self.plot2.time_range = 10

        # Start GUI
        self.show()

    def onSpeedVertical(self):
        # Read slider value
        self.txUnity.speedVertical = 0.01*self.gui.speedVertical.value()

    def onSpeedHorizontal(self):
        # Read slider value
        self.txUnity.speedHorizontal = 0.01*self.gui.speedHorizontal.value()

    def update(self):
        # Elapsed time
        t = time.time() - self.t0

        # Send data to Unity
        self.unity.sendto(self.txUnity, ('127.0.0.1', 4000))

        try:
            # Check for recieved data from Unity
            data, adr = self.unity.recvfrom(1024)

            # Convert data to RxUnity type
            memmove(addressof(self.rxUnity), data, sizeof(self.rxUnity))

        except:
            pass 
       
        # Plot Unity dataG
        self.plot1.update(t, [self.rxUnity.h])
        self.plot2.update(t, [np.rad2deg(self.rxUnity.rotX), np.rad2deg(self.rxUnity.rotZ)])
        

        if self.eMode == E_MOTIONLAB:
            # Pass data on to Motion Lab
            try:
                data, addr = self.plc.recvfrom(1024)
                memmove(addressof(self.rxData), data, sizeof(self.rxData))

            except:
                pass

            # Incerement counter and set Udp Key
            self.txData.iUdpKey = 46505228
            self.txData.iCounter = self.txData.iCounter + 1

            # Apply motion from Unity
            self.txData.em1500_heave_cmd = -self.rxUnity.h
            self.txData.em1500_roll_cmd = self.rxUnity.rotZ
            self.txData.em1500_pitch_cmd = -self.rxUnity.rotX

            # Send data to Motion Lab
            self.plc.sendto(self.txData, ('192.168.90.50', 50050))

        elif self.eMode == E_LABVIEW:
            try:
                data, addr = self.plc.recvfrom(1024)
                memmove(addressof(self.rxData), data, sizeof(self.rxData))

                print(self.rxData.speedVertical)
                print(self.rxData.speedHorizontal)
                
                # LabVIEW to Unity
                self.txUnity = self.rxData

            except:
                pass
                
            # Unity to LabVIEW
            self.txData = self.rxUnity

            # Send data to LabVIEW
            self.plc.sendto(self.txData, ('192.168.90.60', 50161))
            

    def closeEvent(self, event):
        self.unity.close()
        # self.plc.close()

        self.timer.stop()

