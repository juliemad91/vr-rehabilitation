import ctypes

class RxUnity(ctypes.Structure):
    _fields_ = [
        ('h', ctypes.c_float),
        ('rotX', ctypes.c_float),
        ('rotZ', ctypes.c_float)
    ]
    